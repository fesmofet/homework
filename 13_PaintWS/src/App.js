import React from 'react';
import './App.css';
import * as can from './canvas.js';

class App extends React.Component {

    componentDidMount() {
        let canvas = document.getElementById('canvas');
        let color = document.querySelectorAll('.color');


        can.init(canvas, color);
    }
    render() {
        return (
            <div className="main">
                <div>
                    <canvas id="canvas"
                            width="600px"
                            height="400px"
                            onClick={() => this.example}
                            onMouseDown={() => can.mouseDownCanvas()}
                            onMouseUp={() => can.mouseUpCanvas()}
                            onMouseMove={(event) => can.mouseMoveCanvas(event)}
                    />
                    <div>x: <span className="xBlock">0</span></div>
                    <div>y: <span className="yBlock">0</span></div>
                </div>
                <div>
                    <div className="setting">
                        <div className="setting__color">
                            <div className="setting__color-text">Colors:</div>
                            <div className="setting__color-color">
                                <div className="color-red color" onClick={(event) => can.setColor(event)}/>
                                <div className="color-green color" onClick={(event) => can.setColor(event)}/>
                                <div className="color-blue color" onClick={(event) => can.setColor(event)}/>
                            </div>
                        </div>
                        <div className="setting__thickness">
                            <div className="setting__thickness-text">Thickness:</div>
                            <input type="range" min="50" max="150" id="range" onChange={(event) => can.rangeWidth(event)}/>
                        </div>
                    </div>
                </div>
                <button onClick={() => can.clean()}>Очистить</button>
            </div>
        );
    }
}

export default App;

var assert = require('chai').assert;

var checkEvenOrOdd = require('../conditionalStatements').checkEvenOrOdd;
var determineLocOfSpot = require('../conditionalStatements').determineLocOfSpot;
var getSumOfPositive = require('../conditionalStatements').getSumOfPositive;
var countExpression = require('../conditionalStatements').countExpression;
var getGrade = require('../conditionalStatements').getGrade;

describe('Test conditional statements functions', function() {
    //Task 1
    describe('Test checkEvenOrOdd function', function() {

        it('if value 1 is even - should return value1 * value2', function() {
            assert.equal(checkEvenOrOdd(4, 8), 32);
        });

        it('if value 1 is odd - should return value1 + value2', function() {
            assert.equal(checkEvenOrOdd(5, 8), 13);
        });

        it('if value 1 or value2 !== number - should return false', function() {
            assert.equal(checkEvenOrOdd('5', 8), false);
        });

        it('if value 1 or value2 !== number - should return false', function() {
            assert.equal(checkEvenOrOdd({}, null), false);
        });

        it('checkEvenOrOdd should return type number', function(){
            assert.typeOf(checkEvenOrOdd(4, 6), 'number');
        });
    });
    //Task 2
    describe('Test determineLocOfSpot function', function() {

        it('if x < 0 && y > 0 should return 1st quarter', function() {
            assert.equal(determineLocOfSpot (-4, 8), '1st quarter');
        });

        it('if x > 0 && y > 0 should return 2nd quarter', function() {
            assert.equal(determineLocOfSpot (9, 8), '2nd quarter');
        });

        it('if x < 0 && y < 0 should return 3rd quarter', function() {
            assert.equal(determineLocOfSpot (-9, -8), '3rd quarter');
        });

        it('if x > 0 && y < 0 should return 4th quarter', function() {
            assert.equal(determineLocOfSpot (7, -8), '4th quarter');
        });

        it('if x === 0 && y !== 0 should return y axis', function() {
            assert.equal(determineLocOfSpot (0, -8), 'y axis');
        });

        it('if x !== 0 && y === 0) should return x axis', function() {
            assert.equal(determineLocOfSpot (56, 0), 'x axis');
        });

        it('if x === 0 && y === 0 should return cross point', function() {
            assert.equal(determineLocOfSpot (0, 0), 'cross point');
        });

        it('if input not a number return false', function() {
            assert.equal(determineLocOfSpot('45', -4), false);
        });

    });
    //Task 3
    describe('Test getSumOfPositive function', function() {

        it('Sum should be 16', function() {
            assert.equal(getSumOfPositive(10, 4, 2), 16);
        });

        it('Negative numbers Shouldn\'t be counted', function() {
            assert.equal(getSumOfPositive(10, -4, 2), 12);
        });

        it('if input not a number return false', function() {
            assert.equal(getSumOfPositive('10', -4, 2), false);
        });

        it('sumOfPositive should return type number', function(){
            assert.typeOf(getSumOfPositive(4, 6, 10), 'number');
        });

    });
    //Task 4
    describe('Test countExpression function', function() {

        it('if a * b * c > a + b + c return a * b * c  + 3', function() {
            assert.equal(countExpression(10, 4, 4), 163);
        });

        it('if a * b * c < a + b + c return a + b + c  + 3', function() {
            assert.equal(countExpression(10, 8, -40), -19);
        });

        it('if input not a number return false', function() {
            assert.equal(countExpression('10', -4, 2), false);
        });

        it('countExpression should return type number', function(){
            assert.typeOf(countExpression(4, 6, 10), 'number');
        });

    });
    //Task 5
    describe('Test getGrade function', function() {

        it('grade F 0 < 20', function() {
            assert.equal(getGrade(19), 'F');
        });

        it('grade E 20 < 40', function() {
            assert.equal(getGrade(39), 'E');
        });

        it('grade D 40 < 60', function() {
            assert.equal(getGrade(59), 'D');
        });

        it('grade C 40 < 75', function() {
            assert.equal(getGrade(74), 'C');
        });

        it('grade B 75 < 90', function() {
            assert.equal(getGrade(89), 'B');
        });

        it('grade A 90 <= 100', function() {
            assert.equal(getGrade(100), 'A');
        });

        it('if rate is out of range 0 - 100 return Error', function() {
            assert.equal(getGrade(14444), 'Error');
        });

        it('if input not a number return false', function() {
            assert.equal(getGrade('10'), false);
        });

        it('getGrade should take a number and return type string', function(){
            assert.typeOf(getGrade(4), 'string');
        });

    });

});
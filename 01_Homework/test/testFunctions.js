var assert = require('chai').assert;

var getDayName = require('../functions').getDayName;
var findDistance = require('../functions').findDistance;
var numberToText = require('../functions').numberToText;
var stringToNumber = require('../functions').stringToNumber;

describe('Test function tasks', function () {
    //Task 1
    describe('Test getDayName function', function () {
        it('if input is not a number, function should return false', function () {
            assert.equal(getDayName('hello'), false);
        });

        it('if input is <= 0 && input > 7 function should return', function () {
            var answer = 'There are only 7 days in a week. Please, enter the correct day number';
            assert.equal(getDayName(0), answer);
        });

        it('if input is < 0 && input > 7 function should return', function () {
            var answer = 'There are only 7 days in a week. Please, enter the correct day number'
            assert.equal(getDayName(8), answer);
        });

        it('if input is in range 1 - 7 should return day name', function () {
            assert.equal(getDayName(1), 'Monday');
        });

        it('if input is in range 1 -7 should return day name', function () {
            assert.equal(getDayName(7), 'Sunday');
        });
    });
    //Task 2
    describe('Test function findDistance', function () {
        it('the function must take a number in the parameters if not return false', function () {
            assert.equal(findDistance('hello', [], true, {}), false);
        });

        it('function should return square root of expression: (axisX2 - axisX1) ** 2 + (axisY2 - axisY1) ** 2)', function () {
            assert.equal(findDistance(0, 0, 0, 4), 4);
        });

        it('function return square root of expression: (axisX2 - axisX1) ** 2 + (axisY2 - axisY1) ** 2)', function () {
            assert.equal(findDistance(9, -5, 98, 10), 89);
        });
    });
    //Task 3
    describe('Test function numberToText', function () {
        it('the function must take a number in the parameters, if not return false', function () {
            assert.equal(numberToText('hello'), false);
        });

        it('if (number === 0) function should return empty string', function () {
            assert.equal(numberToText(0), '');
        });

        it('if (number <= 19) function should return number in words', function () {
            assert.equal(numberToText(12), 'Twelve');
        });

        it('if number <= 99 function should return number in words', function () {
            assert.equal(numberToText(99), 'Ninety Nine');
        });

        it('if number <= 199 function should return number in words', function () {
            assert.equal(numberToText(158), 'One Hundred Fifty Eight');
        });

        it('if number <= 999 function should return number in words', function () {
            assert.equal(numberToText(999), 'Nine Hundreds Ninety Nine');
        });

        it('if number <= 1999 function should return number in words', function () {
            assert.equal(numberToText(1778), 'One Thousand Seven Hundreds Seventy Eight');
        });

        it('if number <= 999999 function should return number in words', function () {
            assert.equal(numberToText(348412), 'Three Hundreds Forty Eight Thousands Four Hundreds Twelve');
        });

        it('if number <= 1999999 function should return number in words', function () {
            assert.equal(numberToText(1560721), 'One Million Five Hundreds Sixty  Thousands Seven Hundreds Twenty One');
        });

        it('if number <= 999999999 function should return number in words', function () {
            assert.equal(numberToText(989909096), 'Nine Hundreds Eighty Nine Millions Nine Hundreds Nine Thousands Ninety Six');
        });

        it('if number <= 1999999999 function should return number in words', function () {
            assert.equal(numberToText(1999389992), 'One Billion Nine Hundreds Ninety Nine Millions Three Hundreds Eighty Nine Thousands Nine Hundreds Ninety Two');
        });

        it('if number <= 999999999999 function should return number in words', function () {
            assert.equal(numberToText(999999999999), 'Nine Hundreds Ninety Nine Billions Nine Hundreds Ninety Nine Millions Nine Hundreds Ninety Nine Thousands Nine Hundreds Ninety Nine');
        });

        it('if number > 999999999999 function should return ', function () {
            assert.equal(numberToText(9999999999999), 'Sorry, but I still have to write tests, so that\'s all.');
        });

        it('if number < 0 function should return negative number in words', function () {
            assert.equal(numberToText(-909), 'Minus Nine Hundreds Nine');
        });
    });
    //Task 4
    describe('Test function stringToNumber', function () {
        it('the function must take a string in the parameters, if not return false', function () {
            assert.equal(stringToNumber(1), false);
        });

        it('string "one" should return 1', function () {
            assert.equal(stringToNumber('onE'), 1);
        });

        it('string "fifty" should return 1', function () {
            assert.equal(stringToNumber('fiFty'), 50);
        });

        it('string "fifty four" should return 1', function () {
            assert.equal(stringToNumber('fiftY four'), 54);
        });

        it('string "one hundred" should return 100', function () {
            assert.equal(stringToNumber('ONE hundred'), 100);
        });

        it('string "two hundreds twelve" should return 212', function () {
            assert.equal(stringToNumber('two hundreds twelve'), 212);
        });

        it('string "eight hundreds sixty" should return 860', function () {
            assert.equal(stringToNumber('eight hundreds sixty'), 860);
        });

        it('string "five hundreds ninety four" should return 594', function () {
            assert.equal(stringToNumber('five hundreds ninety four'), 594);
        });

        it('string "nine hundreds ninety nine" should return 999', function () {
            assert.equal(stringToNumber('niNe huNdreds ninetY nine'), 999);
        });
    });
});
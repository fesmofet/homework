var assert = require('chai').assert;

var findMinElArr = require('../arrays').findMinElArr;
var findMaxElArr = require('../arrays').findMaxElArr;
var findIndexOfMin = require('../arrays').findIndexOfMin;
var findIndexOfMax = require('../arrays').findIndexOfMax;
var calcSumOddIndex  = require('../arrays').calcSumOddIndex;
var reverseArr = require('../arrays').reverseArr;
var calcSumOddIndexEl = require('../arrays').calcSumOddIndexEl;
var halfReverse = require('../arrays').halfReverse;
var sortBubble = require('../arrays').sortBubble;
var sortSelect = require('../arrays').sortSelect;
var sortInsert = require('../arrays').sortInsert;
var sortMerge = require('../arrays').sortMerge;
var sortQuick = require('../arrays').sortQuick;

describe('Test array functions', function () {
    //Task 1
    describe('Test function findMinElArr', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(findMinElArr('hi'), false);
        });

        it('the function should find the minimum array object', function () {
            assert.equal(findMinElArr([9, 0, 3, 10]), 0)
        });

        it('the function should find the minimum array object', function () {
            assert.equal(findMinElArr([9, 0, 3, -10]), -10)
        });

        it('the function should find the minimum array object', function () {
            assert.equal(findMinElArr(['hi', 7003, 98]), 'hi')
        });
    });
    // Task 2
    describe('Test function findMaxElArr', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(findMaxElArr('hi'), false);
        });

        it('the function should find the maximum array object', function () {
            assert.equal(findMaxElArr([9, 0, 3, 10]), 10)
        });

        it('the function should find the maximum array object', function () {
            assert.equal(findMaxElArr([9, 0, 3, -10]), 9)
        });

        it('the function should find the maximum array object', function () {
            assert.equal(findMaxElArr(['hi', -7003, -98]), 'hi')
        });
    });
    //Task 3
    describe('Test function findIndexOfMin', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(findIndexOfMin('hi'), false);
        });

        it('the function should find index of the maximum array object', function () {
            assert.equal(findIndexOfMin([9, 0, 3, 10]), 1);
        });

        it('the function should find index of the maximum array object', function () {
            assert.equal(findIndexOfMin([9, 0, 3, -10]), 3);
        });

        it('the function should find index of the maximum array object', function () {
            assert.equal(findIndexOfMin(['hi', 7003, 98]), 0);
        });
    });
    //Task 4
    describe('Test function findIndexOfMax', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(findIndexOfMax('hi'), false);
        });

        it('the function should find index of the maximum array object', function () {
            // console.log(findIndexOfMax([9, 0, 3, 10]))
            assert.equal(findIndexOfMax([9, 0, 3, 10]), 3);
        });

        it('the function should find the index of maximum array object', function () {
            assert.equal(findIndexOfMax([9, 0, 3, -10]), 0);
        });

        it('the function should find the index of maximum array object', function () {
            assert.equal(findIndexOfMax([7, 8, 98, 8, 6]), 2);
        });
    });
    //Task 5
    describe('Test function calcSumOddIndex', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(calcSumOddIndex('hi'), false);
        });

        it('the function should calculate the sum of elements of an array with odd indices', function () {
            assert.equal(calcSumOddIndex([9, 0, 3, 10]), 10);
        });

        it('the function should calculate the sum of elements of an array with odd indices', function () {
            assert.equal(calcSumOddIndex([9, 2, 3, 10, 45, 10]), 22);
        });

        it('the function should calculate the sum of elements of an array with odd indices', function () {
            assert.equal(calcSumOddIndex([9, 2, 3, 10, 45, -10]), 2);
        });
    });
    //Task 6
    describe('Test function reverseArr', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(reverseArr(3), false);
        });

        it('function should return reverse array', function () {
            var arr1 = [1, 2, 3, 4, 5];
            var arr2 = [5, 4, 3, 2, 1];
            assert.sameOrderedMembers(reverseArr(arr1), arr2);
        });

        it('function should return reverse array2', function () {
            var arr1 = ['hi', 8, 9, 10, 11];
            var arr2 = [11, 10, 9, 8, 'hi'];
            assert.deepEqual(reverseArr(arr1), arr2);
        });
    });
    //Task 7
    describe('Test function calcSumOddIndexEl', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(calcSumOddIndexEl('hi'), false);
        });

        it('the function should count the number of odd array elements', function () {
            assert.equal(calcSumOddIndexEl([9, 0, 3, 10]), 2);
        });

        it('the function should count the number of odd array elements', function () {
            assert.equal(calcSumOddIndexEl([9, 2, 3, 10, 45, 10]), 3);
        });

        it('the function should count the number of odd array elements', function () {
            assert.equal(calcSumOddIndexEl([9, 2]), 1);
        });
    });
    //Task 8
    describe('Test function halfReverse', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(halfReverse(3), false);
        });

        it('function should return reverse half array', function () {
            var arr1 = [1, 2, 3, 4, 5];
            var arr2 = [4, 5, 3, 1, 2];
            assert.sameOrderedMembers(halfReverse(arr1), arr2);
        });

        it('function should return reverse half array2', function () {
            var arr1 = ['hi', 8, 9, 10, 11];
            var arr2 = [10, 11, 9, 'hi', 8];
            assert.deepEqual(halfReverse(arr1), arr2);
        });
    });
    //Task 9
    describe('Test function sortBubble', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(sortBubble(3), false);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5];
            var arr2 = [1, 3, 5, 7, 9];
            assert.sameOrderedMembers(sortBubble(arr1), arr2);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5, -7, 300];
            var arr2 = [-7, 1, 3, 5, 7, 9, 300];
            assert.sameOrderedMembers(sortBubble(arr1), arr2);
        });
    });

    describe('Test function sortSelect', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(sortSelect(3), false);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5];
            var arr2 = [1, 3, 5, 7, 9];
            assert.sameOrderedMembers(sortSelect(arr1), arr2);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5, -7, 300];
            var arr2 = [-7, 1, 3, 5, 7, 9, 300];
            assert.sameOrderedMembers(sortSelect(arr1), arr2);
        });
    });

    describe('Test function sortInsert', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(sortInsert(3), false);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5];
            var arr2 = [1, 3, 5, 7, 9];
            assert.sameOrderedMembers(sortInsert(arr1), arr2);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [1, 9, 3, 7, 5, -7, 300];
            var arr2 = [-7, 1, 3, 5, 7, 9, 300];
            assert.sameOrderedMembers(sortInsert(arr1), arr2);
        });
    });
    //Task 10
    describe('Test function sortMerge', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(sortMerge(3), false);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [2, 5, 1, 3, 7, 2, 3, 8, 6, 3];
            var arr2 = [1, 2, 2, 3, 3, 3, 5, 6, 7, 8];
            assert.sameOrderedMembers(sortMerge(arr1), arr2);
        });

        it('function should return sorted array in ascending order ', function () {
            var arr1 = [2, 5, 109, 3, 0, -5, 3, 8, 6, 3, -7, 30, 5];
            var arr2 = [-7, -5, 0, 2, 3, 3, 3, 5, 5, 6, 8, 30, 109];
            assert.sameOrderedMembers(sortMerge(arr1), arr2);
        });
    });

    describe('Test function sortQuick', function () {
        it('if input !== array, function should return false', function () {
            assert.equal(sortQuick(3), false);
        });

        it('function should return sorted array in ascending order', function () {
            var arr1 = [2, 5, 1, 3, 7, 2, 3, 8, 6, 3];
            var arr2 = [1, 2, 2, 3, 3, 3, 5, 6, 7, 8];
            assert.sameOrderedMembers(sortQuick(arr1), arr2);
        });

        it('function should return sorted array in ascending order ', function () {
            var arr1 = [2, 5, 109, 3, 0, -5, 3, 8, 6, 3, -7, 30, 5];
            var arr2 = [-7, -5, 0, 2, 3, 3, 3, 5, 5, 6, 8, 30, 109];
            assert.sameOrderedMembers(sortQuick(arr1), arr2);
        });
    });
});
let canv = document.getElementById('canvas');
let ctx = canv.getContext('2d');
let pi = Math.PI;
let colorValue = 'red';
let thickValue = '10';
let isMouseDown = false;
let coords = [];
let xBlock;
let yBlock;
let color = document.querySelectorAll('.color');
let range = document.getElementById('range');
// let isDrawing = false;

range.addEventListener('change', function(){
    thickValue = range.value / 10;
});

color.forEach(function(element, index, array){
    element.addEventListener('click', function(){
        color.forEach(function(element, index, array){
            element.style.border = 'none';
        });
        switch(element.classList[0]){
            case 'color-red':
                colorValue = 'red';
                element.style.border = '2px solid black';
                break;
            case 'color-green':
                element.style.border = '2px solid black';
                colorValue = 'green';
                break;
            case 'color-blue':
                element.style.border = '2px solid black';
                colorValue = 'blue';
                break;
        }
    });
});

canv.addEventListener('mousedown', function(){
    isMouseDown = true;
});



canv.addEventListener('mouseout', function(){
    isMouseDown = false;
    ctx.beginPath();
});

canv.addEventListener('mouseup', function(){
    isMouseDown = false;
    ctx.beginPath();
    coords.push('mouseup');
});

canv.addEventListener('mousemove', function(e){
    xBlock = document.getElementById('x');
    yBlock = document.getElementById('y');
    xBlock.innerHTML = e.offsetX.toString();
    yBlock.innerHTML = e.offsetY.toString();




    if(isMouseDown){
        coords.push([e.offsetX, e.offsetY]);

        ctx.strokeStyle = colorValue;
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.lineWidth = thickValue;
        ctx.stroke();

        ctx.beginPath();
        ctx.fillStyle = colorValue;
        ctx.arc(e.offsetX, e.offsetY, thickValue/2, 0, pi * 2);
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(e.offsetX, e.offsetY);
    }
});
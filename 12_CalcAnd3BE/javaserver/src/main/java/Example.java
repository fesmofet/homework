import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

class Request {
    private float firstNumber;
    private float secondNumber;
    private String action;

    public Request() {

    }

    public Request(float firstNumber, float secondNumber, String action) {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
        this.action = action;
    }

    public float getSecondNumber() {
        return secondNumber;
    }

    public void setSecondNumber(float secondNumber) {
        this.secondNumber = secondNumber;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public float getFirstNumber() {
        return firstNumber;
    }

    public void setFirstNumber(float firstNumber) {
        this.firstNumber = firstNumber;
    }
}

@RestController
@EnableAutoConfiguration
@RequestMapping("/java")
public class Example {
    @PostMapping()
    public float xz(@RequestBody Request request) {

        float result = calc(request.getFirstNumber(), request.getSecondNumber(), request.getAction());

        return result;
    }

    private float calc(float a, float b, String oper) {
        float result = 0;
        switch (oper) {
            case "plus": result = a + b; break;
            case "minus": result = a - b; break;
            case "multiple": result = a * b; break;
            case "devide": result =  a / b; break;
        }
        return result;
    }

    public static void main(String[] args) {
        SpringApplication.run(Example.class, args);
    }

}
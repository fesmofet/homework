var assert = require('chai').assert;

var sumFrom1To99 = require('../loops').sumFrom1To99;
var checkPrimeNumber = require('../loops').checkPrimeNumber;
var findSquareRoot = require('../loops').findSquareRoot;
var findSquareRoot2 = require('../loops').findSquareRoot2;
var findFactorial = require('../loops').findFactorial;
var calcSumOfDigits = require('../loops').calcSumOfDigits;
var getMirrorNumber = require('../loops').getMirrorNumber;

describe('Testing loops functions', function () {
    //Task 1
    describe('Testing function sumFrom1To99', function () {
        it('function should return sum of al int from 1  to 99', function () {
            assert.equal(sumFrom1To99(), 4950);
        });

        it('function should return a number', function () {
            assert.typeOf(sumFrom1To99(), 'number');
        });
    });
    //Task 2
    describe('Testing function checkPrimeNumber', function () {
        it('function should return prime number', function () {
            assert.equal(checkPrimeNumber(29), 'prime number');
        });

        it('function should return prime number', function () {
            assert.equal(checkPrimeNumber(22), 'not prime number');
        });

        it('function should return false when number is negative', function () {
            assert.equal(checkPrimeNumber(-22), false);
        });

        it('function should return false input is not a number', function () {
            assert.equal(checkPrimeNumber('Hi!'), false);
        });

        it('function should return a string', function () {
            assert.typeOf(checkPrimeNumber(2), 'string');
        });
    });
    //Task 3
    describe('Testing function findSquareRoot', function () {

        it('function should return false input is not a number', function (){
            assert.equal(findSquareRoot('Hi!'), false);
        });

        it('function should return a number', function () {
            assert.typeOf(findSquareRoot(35), 'number');
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot(35), 5)
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot(67), 8)
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot(1012), 31)
        });
    });

    describe('Testing function findSquareRoot2', function () {

        it('function should return false input is not a number', function () {
            assert.equal(findSquareRoot2('Hi!'), false);
        });

        it('function should return a number', function () {
            assert.typeOf(findSquareRoot2(35), 'number');
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot2(35), 5)
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot2(67), 8)
        });

        it('function should find the root of a natural number up to an integer', function () {
            assert.equal(findSquareRoot2(1012), 31)
        });
    });
    //Task 4
    describe('Testing function findFactorial', function () {
        it('function should return factorial of value', function () {
            assert.equal(findFactorial(5), 120);
        });

        it('function should return factorial of value', function () {
            assert.equal(findFactorial(7), 5040);
        });

        it('function should return false input is not a number', function () {
            assert.equal(findFactorial('Hi!'), false);
        });

        it('function should return a number', function () {
            assert.typeOf(findFactorial(2), 'number');
        });
    });
    //Task 5
    describe('Testing function calcSumOfDigits', function () {
        it('function should return sum of digits in value', function () {
            assert.equal(calcSumOfDigits(567), 18);
        });

        it('function should return sum of digits in value', function () {
            assert.equal(calcSumOfDigits(687), 21);
        });

        it('function should return false if input is not a number', function () {
            assert.equal(calcSumOfDigits('Hi!'), false);
        });

        it('function should return a number', function () {
            assert.typeOf(calcSumOfDigits(2986), 'number');
        });
    });
    //Task 6
    describe('Testing function getMirrorNumber', function () {
        it('function should return sum of digits in value', function () {
            assert.equal(getMirrorNumber(567), 765);
        });

        it('function should return sum of digits in value', function () {
            assert.equal(getMirrorNumber(687), 786);
        });

        it('function should return false if input is not a number', function () {
            assert.equal(getMirrorNumber('Hi!'), false);
        });

        it('function should return a number', function () {
            assert.typeOf(getMirrorNumber(2986), 'number');
        });
    });
});
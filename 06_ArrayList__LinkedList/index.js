var IList = function () {};

IList.prototype.init = () => {}; // метод для инициализации var array = new ArrayList(); array.init([1,2,3,4,5])
IList.prototype.toString = () => {}; // +
IList.prototype.getSize = () => {}; // +
IList.prototype.push = (value) => {}; // +
IList.prototype.pop = () => {};
IList.prototype.shift = () => {};
IList.prototype.unshift = (value) => {};
IList.prototype.slice = (start, end) => {};
IList.prototype.splice = (start, numberToSplice, ...elements) => {};
IList.prototype.sort = (comparator) => {}; // comparator ==> callback
IList.prototype.get = (index) => {}; // get by index
IList.prototype.set = (index, element) => {}; // set element by index

//ArrayList
var ArrayList = function() {
    IList.apply(this, arguments);
    this.array = [];
};
ArrayList.prototype = Object.create(IList.prototype);
ArrayList.prototype.constructor = ArrayList;

ArrayList.prototype.push = function(value) {
    this.array[this.array.length] = value;
    return this.array.length;
    
};
ArrayList.prototype.getSize = function(value) {
    return this.array.length;
};
ArrayList.prototype.init = function(initialArray) {
    for (var i = 0; i < initialArray.length; i++) {
        this.push(initialArray[i]);
        
    }
};
ArrayList.prototype.toString = function () {
    var string = '';
    for(var i = 0; i < this.array.length; i ++) {
        this.array.length-1 === i ? string += this.array[i] : string += this.array[i] + ',';     
    }
    
    return string
}

ArrayList.prototype.pop = function() {
    var lastElementInArray = this.array[this.array.length - 1];
    this.array.length = this.array.length - 1;
    return lastElementInArray;
}

ArrayList.prototype.shift = function() {
    var firstElementInArray = this.array[0];
    delete this.array[0];
    for (var i = 0; i < this.array.length; i++) {
        this.array[i] = this.array[i + 1];
    }
    this.array.length = this.array.length - 1;
    return firstElementInArray;
}

ArrayList.prototype.unshift = function(value) {
    this.array.length = this.array.length + 1;
    for (var i = this.array.length - 1; i > 0; i--) {
        this.array[i] = this.array[i - 1];
    }
    this.array[0] = value; 
    return this.array.length;
}

ArrayList.prototype.slice = function(start, end) {
    var sliceArray = [];//new ArrayList()
    var spliceEnd = end || this.array.length;
    for (var i = start; i < spliceEnd; i++) {
        sliceArray.push(this.array[i]);
    }
    return sliceArray
}

ArrayList.prototype.splice = function(start, deleteCounter) {
    var tmpArray = [];//new ArrayList()
    var removedArray = [];//new ArrayList()

    if (start > 0) {
        for (var i = 0; i < start; i++) {
            tmpArray.push(this.array[i]);
        }
    }

    for (var i = start; i < deleteCounter + start; i++) {
        removedArray.push(this.array[i]);
    }

    if(arguments.length > 0) {
        for (var i = 2; i < arguments.length; i++) {
            tmpArray.push(arguments[i]);
        }
    }

    for (var i = deleteCounter + start; i < this.array.length; i++) {
        tmpArray.push(this.array[i]);
    }
    
    this.array = tmpArray
    return removedArray
}

ArrayList.prototype.sort = function() {
    for (var i = 0; i < this.array.length - 1; i++) {
        for (var j = 0; j < this.array.length - 1 - i; j++) {
            if (this.array[j] > this.array[j+1]) {
                var tmp = this.array[j+1]
                this.array[j+1] = this.array[j]
                this.array[j] = tmp
            }
        }
    }
    return this.array
}

ArrayList.prototype.get = function(index) {
    return this.array[index];
};

ArrayList.prototype.set = function(index, element) {
    this.array[index] = element;
    return this.array;
};

var arr = new ArrayList();
arr.init([2, 5, 1, 4, 1]);

// LinkedList
var Node = function(value) {
    this.value = value;
    this.next = null;
};
var LinkedList = function() {
    IList.apply(this, arguments);
    this.root = null;
};
LinkedList.prototype = Object.create(IList.prototype);
LinkedList.prototype.constructor = LinkedList;

LinkedList.prototype.getSize = function() {
    var tempNode = this.root;
    var size = 0;

    while(tempNode !== null) {
        tempNode = tempNode.next;
        size++;
    }
    return size;
};
LinkedList.prototype.unshift = function(value) {
    var size = this.getSize();
    var node = new Node(value);

    node.next = this.root;
    this.root = node;

    return size + 1;
};
LinkedList.prototype.init = function(initialArray) {
    for (var i = initialArray.length -1 ; i >=0; i--) {
        this.unshift(initialArray[i]);
    }
};

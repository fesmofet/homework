window.onload = function() {

    var minValue = document.querySelector("#min");
    var maxValue = document.querySelector("#max");
    var output = document.querySelector("#output");
    var winners = [];

     function getRandom () {
        var min  = Number(minValue.value);
        var max  = Number(maxValue.value) + 1;

        if (winners.length === (max - min)) {
            output.value = "All available numbers has been randomized";
            document.getElementById("output").disabled = true;
            document.querySelector('#btn').classList.remove('active', 'hover');
            return;
        }

        var randomInteger = generateRandomInt(min,max);

        if(checkElementInArray(randomInteger, winners)) {
            getRandom(min,max);
        } else {
            winners.push(randomInteger);
            output.value = randomInteger;
        }
    }

    function generateRandomInt(min,max) {
        return Math.floor(Math.random() * (max - min) + min);
    }

    function checkElementInArray(value, array) {

        for(var i = 0; i < array.length; i++) {
            if(array[i] === value) {
                return true;
            }
        }
        return false;
    }

    function resetFields() {
        winners = [];
        minValue.value = "";
        maxValue.value = "";
        output.value = "";
        minValue.disabled = false;
        maxValue.disabled = false;
        document.querySelector('#btn').classList.add('active', 'hover');
        document.querySelector("#output").disabled = true;
    }
    document.querySelector('#btn').addEventListener("click", function () {
        if (minValue.value === "" && maxValue.value === "") {
            return alert('all fields must be filled');
        } else if (minValue.value === "") {
            return alert('Please enter min value');
        } else if (maxValue.value === "") {
            return alert('Please enter max value');
        } else {
            minValue.disabled = true;
            maxValue.disabled = true;
            return getRandom();
        }
    });
     document.querySelector('#btn2').onclick = resetFields;
};

module.exports = {
    //Task 1
    getDayName: function (number) {
        if (typeof number !== 'number') {
            return false;
        }

        switch(number) {
            case 1:
                return 'Monday';
            case 2:
                return 'Tuesday';
            case 3:
                return 'Wednesday';
            case 4:
                return 'Thursday';
            case 5:
                return 'Friday';
            case 6:
                return 'Saturday';
            case 7:
                return 'Sunday';
            default:
                return 'There are only 7 days in a week. Please, enter the correct day number'
        }
    },
    //Task 2
    findDistance: function (axisX1, axisX2, axisY1, axisY2) {
        if (typeof axisX1 !== 'number' || typeof axisX2 !== 'number' || typeof axisY1 !== 'number' || typeof axisY2 !== 'number') {
            return false;
        }

        return Math.floor(Math.sqrt((axisX2 - axisX1) ** 2 + (axisY2 - axisY1) ** 2));
    },
    //Task 3
    numberToText: function call(number) {
        if (typeof number !== 'number') {
            return false;
        }
        if (number < 0) {
            return  'Minus ' + call(-number);
        } else if (number === 0) {
            return  '';
        } else if (number <= 19) {
            var arr19 = ['One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight',
                'Nine', 'Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen',
                'Seventeen', 'Eighteen', 'Nineteen'];
            return  arr19[number-1] ;
        } else if (number <= 99) {
            var arrDozens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy',
                'Eighty', 'Ninety'];
            return  arrDozens[Math.floor(number / 10 - 2)] + ' ' + call(number % 10);
        } else if (number <= 199) {
            return  'One Hundred ' + call(number % 100);
        } else if (number <= 999) {
            return  call(Math.floor(number / 100)) + ' Hundreds ' + call(number % 100);
        } else if (number <= 1999) {
            return  'One Thousand ' + call(number % 1000);
        } else if (number <= 999999) {
            return  call(Math.floor(number / 1000)) + ' Thousands ' + call(number % 1000);
        } else if (number <= 1999999) {
            return  'One Million ' + call(number % 1000000);
        } else if (number <= 999999999) {
            return  call(Math.floor(number / 1000000)) + ' Millions ' + call(number % 1000000);
        } else if (number <= 1999999999) {
            return  'One Billion ' + call(number % 1000000000);
        } else if (number <= 999999999999) {
            return  call(Math.floor(number / 1000000000)) + ' Billions ' + call(number % 1000000000);
        } else if (number > 999999999999) {
            return 'Sorry, but I still have to write tests, so that\'s all.'
        }

    },
    //Task 4
    stringToNumber: function (string) {
        if (typeof string !=='string') {
            return false;
        }
        function parseStr(string) {
            var array = [];
            var j = 0;
            for (i = 0; i < string.length; i++) {
                if (string[i] === ' ') {
                    j++;
                } else {
                    array[j] ? array[j] += string[i] : array[j] = string[i];
                }
            }
            return array;
        }
        var strToLow = string.toLowerCase();
        var array = parseStr(strToLow);


        var oneWord = {
            'null': 0, 'zero': 0,'one': 1, 'two': 2,'three': 3,'four': 4, 'five': 5, 'six': 6, 'seven': 7, 'eight': 8, 'nine': 9,
            'ten': 10, 'eleven': 11, 'twelve': 12, 'thirteen': 13, 'fourteen': 14, 'fifteen': 15, 'sixteen': 16,
            'seventeen': 17, 'eighteen': 18, 'nineteen': 19, 'twenty': 20,'thirty': 30, 'forty': 40, 'fifty': 50,
            'sixty': 60, 'seventy': 70, 'eighty': 80, 'ninety': 90
        };
        var twoWords = {
            'one': 1, 'two': 2,'three': 3,'four': 4, 'five': 5, 'six': 6, 'seven': 7, 'eight': 8, 'nine': 9, 'hundred': 100,
            'hundreds': 100
        };
        var sumNumbers = oneWord[array[0]] + twoWords[array[1]];
        var multiplyNumbers = oneWord[array[0]] * twoWords[array[1]];
        var sumNumbers2 = oneWord[array[2]] + twoWords[array[3]];

        if (array.length === 1) {
            return oneWord[array[0]];
        } else if (array.length === 2) {
            return sumNumbers <= 99 ? sumNumbers : multiplyNumbers;
        } else if (array.length === 3) {
            return multiplyNumbers + oneWord[array[2]];
        } else if (array.length === 4) {
            return multiplyNumbers + sumNumbers2;
        }
    },

};
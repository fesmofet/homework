window.onload = function () {

    document.querySelector("#add").addEventListener('click', addStudent);
    document.querySelector("#init").addEventListener('click', initApp);
    document.querySelector("#race").addEventListener('click', startRace);
    document.querySelector("#btnFirstName").addEventListener('click', fillName);
    document.querySelector("#btnMarkName").addEventListener('click', fillMark);
    document.querySelector("#btnAgeName").addEventListener('click', fillAge);
    document.querySelector("#btnSkillsName").addEventListener('click', fillSkills);
    document.querySelector("#btnEnglishName").addEventListener('click', fillLanguage);
    document.querySelector("#genarateGame").addEventListener('click', genarateGame);
};

const persons = [];
let counter = 0;
let canvasNumber = 0;

function fillName() {
    document.querySelector("#firstName").value = generateName();
}

function generateName() {
    let names = ["Сергей", "Андрей", "Богдан", "Коля", "Вася", "Петя", "Гепа",
        "Сергей2", "Андрей2", "Богдан2", "Коля2", "Вася2", "Петя2", "Гепа2",
        "Сергей3", "Андрей3", "Богдан3", "Коля3", "Вася3", "Петя3", "Гепа3"];
    let index = getRandom(0, 20);
    return names[index];
}

function fillMark() {
    document.querySelector("#mark").value = generateMark();
}

function generateMark() {

    return getRandom(3, 5);
}

function fillAge() {
    document.querySelector("#age").value = generateAge();
}

function generateAge() {
    return getRandom(18, 35);
}

function fillSkills() {
    document.querySelector("#skills").value = generateSkills().toString();
}

function generateSkills() {
        return getRandom(1, 10) <=8 ? ["JS", "Worpress", "React"] : ["Worpress"];
}

function fillLanguage() {
    document.querySelector("#english").value = generateLanguage();
}

function generateLanguage() {
    return (getRandom(0, 10) <= 5) ? "C1" : "A1";
}

function genarateGame() {

    for (var i = 0; i < 5; i++) {
        var person = {
            name: generateName(),
            mark: generateMark(),
            age: generateAge(),
            skills: generateSkills(),
            english: generateLanguage(),
            id: i+1
        };

        console.log(person.skills)
        persons.push(person);
        drawElement(person);
    }
    initApp();
}

function addStudent() {
    counter++;
    let name = document.querySelector("#firstName").value;
    let mark = document.querySelector("#mark").value;
    let age = document.querySelector("#age").value;
    let skills = document.querySelector("#skills").value;
    let english = document.querySelector("#english").value;
    let id = counter;
    let person = {
        name, mark, age, skills, english, id
    };
    persons.push(person);
    drawElement(person);
}

function drawElement(person) {
    let wrapper = document.querySelector(".race-wrapper");
    const div = document.createElement('div');
    div.className = 'student-progress-flow';
    div.innerHTML = `
        <div class="name">
             <div id="name" width="70" height="70">${person.name}</div>
        </div>
        <div class="space">
             <div id="space" width="20" height="10"> >>>> </div>
        </div>
        <div class="stage1">
            <canvas id="my_canvas${++canvasNumber}" title = "Пришел на курсы" width="70" height="70"></canvas>
        </div>
        <div class="stage2">
            <canvas id="my_canvas${++canvasNumber}"  title = "Получил сертфикат" width="70" height="70"></canvas>
            <div class ="error${canvasNumber}"> Получил низкую оценку </div>
        </div>
        <div class="stage3">
            <canvas id="my_canvas${++canvasNumber}" title = "Пошел на собеседование" width="70" height="70"></canvas>
        </div>
        <div class="stage4">
            <canvas id="my_canvas${++canvasNumber}" title = "Поговорил с HR" width="70" height="70"></canvas>
            <div class ="error${canvasNumber}"> Не прошел по возрасту </div>
            <canvas id="my_canvas${++canvasNumber}" title = "Поговорилс тим лидом" width="70" height="70"></canvas>
            <div class ="error${canvasNumber}"> Не хватило скилов  </div>
            <canvas id="my_canvas${++canvasNumber}" title = "Поговорил с клиентом" width="70" height="70"></canvas>
            <div class ="error${canvasNumber}"> Хреново с английским </div>
        </div>
  `;
    wrapper.appendChild(div);
}

function initApp() {
    persons.forEach(element => initPlayer(element))
}

function getCanvasId(personId) {

    switch (personId) {
        case 1: {
            canvasId = 1;
            break;
        }
        case 2: {
            canvasId = 7;
            break;
        }
        case 3: {
            canvasId = 13;
            break;
        }
        case 4: {
            canvasId = 19;
            break;
        }
        case 5: {
            canvasId = 25;
            break;
        }
    }
    return canvasId;
}

function initPlayer(person) {
    id = getCanvasId(person.id);
    ctx = document.querySelector(`#my_canvas${id}`).getContext('2d');
    var al = 100;
    var start = 4.72;
    var cw = ctx.canvas.width;
    var ch = ctx.canvas.height;
    var diff = 100;
    diff = ((al / 100) * Math.PI * 2 * 10).toFixed(2);
    ctx.clearRect(0, 0, cw, ch);
    ctx.lineWidth = 10;
    ctx.fillStyle = 'rgb(0,0,0)';
    ctx.strokeStyle = 'rgba(139,126,78,0.18)';
    ctx.textAlign = 'center';
    ctx.beginPath();
    ctx.arc(35, 35, 30, start, diff / 10 + start, false);
    ctx.stroke();
}

function startRace() {
    switch (persons.length) {
        case 1: {
            Promise.race([goToJsCourse(persons[0]).then(getCertificat).then(goToInterview)]).catch(error => console.error(error)).then(name => {
                alert(`${name} WIN!!!`)
            });
            break;
        }
        case 2: {
            Promise.race([goToJsCourse(persons[0]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[1]).then(getCertificat).then(goToInterview)]).then(name => {
                alert(`${name} WIN!!!`)
            });
            break;
        }
        case 3: {
            Promise.race([goToJsCourse(persons[0]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[1]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[2]).then(getCertificat).then(goToInterview),]).then(name => {
                alert(`${name} WIN!!!`)
            }).catch(error => console.error(error));
            break;
        }
        case 4: {
            Promise.race([goToJsCourse(persons[0]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[1]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[2]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[3]).then(getCertificat).then(goToInterview)]).then(name => {
                alert(`${name} WIN!!!`)
            }).catch(error => console.error(error));
            break;
        }
        case 5: {
            Promise.race([goToJsCourse(persons[0]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[1]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[2]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[3]).then(getCertificat).then(goToInterview),
                goToJsCourse(persons[4]).then(getCertificat).then(goToInterview)]).then(name => {
                alert(`${name} WIN!!!`)
            }).catch(error => console.error(error));
            break;
        }
    }
}

function stopBar(ctx, internal) {
    var al = 0;
    var start = 4.72;
    var cw = ctx.canvas.width;
    var ch = ctx.canvas.height;
    var diff = 100;

    function progressSim() {
        ctx.clearRect(0, 0, cw, ch);
        ctx.lineWidth = 10;
        ctx.fillStyle = '#09F';
        ctx.strokeStyle = "#09F";
        ctx.textAlign = 'center';
        ctx.fillText(al + '%', cw * .5, ch * .5 + 2, cw);
        ctx.beginPath();
        ctx.arc(35, 35, 30, start, diff / 10 + start, false);
        ctx.stroke();
        if (al >= 100) {
            ctx.fillStyle = '#ff2513';
            ctx.strokeStyle = "#ff2209";
            ctx.fill();
            ctx.stroke();
            clearTimeout(sim);
        }
        al++;
    }

    var sim = setInterval(progressSim, 0);
}
function startBar(ctx, internal) {
    var al = 0;
    var start = 4.72;
    var cw = ctx.canvas.width;
    var ch = ctx.canvas.height;
    var diff;

    function progressSim() {
        diff = ((al / 100) * Math.PI * 2 * 10).toFixed(2);
        ctx.clearRect(0, 0, cw, ch);
        ctx.lineWidth = 10;
        ctx.fillStyle = '#09F';
        ctx.strokeStyle = "#09F";
        ctx.textAlign = 'center';
        ctx.fillText(al + '%', cw * .5, ch * .5 + 2, cw);
        ctx.beginPath();
        ctx.arc(35, 35, 30, start, diff / 10 + start, false);
        ctx.stroke();
        if (al >= 100) {
            ctx.fillStyle = '#4aff0b';
            ctx.strokeStyle = "#4aff0b";
            ctx.fill();
            ctx.stroke();
            clearTimeout(sim);
        }
        al++;
    }

    var sim = setInterval(progressSim, internal);
}

function getRandom(min, max) {
    return Math.floor(Math.random() * (+max - +min)) + +min;
}

function goToJsCourse(person) {
    //const delay = getRandom(1, 2) * 10;
     const delay = getRandom(5, 10) * 10;
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id)}`).getContext('2d');
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {
        setTimeout(() => resolve(person), delay * 100);
    })
}

function getCertificat(person) {
    //const delay = getRandom(1, 2) * 10;
    const delay = getRandom(2, 4) * 10;
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 1}`).getContext('2d')
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {
        if (person.mark >= 4) {
            setTimeout(() => resolve(person), delay * 100);
        } else {
            setTimeout(() => {
                ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 1}`).getContext('2d')
                stopBar(ctx, 0);
            }, delay * 100);
        }
        // {
        //     setTimeout(() => {
        //           document.querySelector(`#my_canvas${getCanvasId(person.id) + 1}`).remove();
        //       document.querySelector(`.error${getCanvasId(person.id) + 1}`).style.display = "block";
        //     }, delay * 100);
        // }
    })
}

function goToInterview(person) {
   // const delay = getRandom(1, 2) * 10;
     const delay = getRandom(2, 4) * 10;
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 2}`).getContext('2d')
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {
        setTimeout(() => Promise.all([hRInterview(person), projectInterview(person), clientInterview(person)]).then(() => resolve(person.name)), delay * 100);
    })
}

function hRInterview(person) {
    // const delay = getRandom(1, 2) * 10;
     const delay = getRandom(6, 8) * 10;
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 3}`).getContext('2d')
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {
        if (person.age <= 30 && person.age >= 18) {
            setTimeout(() => resolve(person), delay * 100);
        } else
        {
            setTimeout(() => {
                ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 3}`).getContext('2d')
                stopBar(ctx, 0);
            }, delay * 100);
        }
        //     {
        //         setTimeout(() => document.querySelector(`#my_canvas${getCanvasId(person.id) + 3}`).remove(), delay * 100);
        //     setTimeout(() => document.querySelector(`.error${getCanvasId(person.id) + 3}`).style.display = "block", delay * 100);
        // }
    })
}

function projectInterview(person) {
    // const delay = getRandom(1, 2) * 10;
       const delay = getRandom(10, 20) * 10;
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 4}`).getContext('2d')
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {

        console.log(person.skills);
        if (person.skills.length > 2) {
            setTimeout(() => resolve(person), delay * 100);
        } else
        {
            setTimeout(() => {
                ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 4}`).getContext('2d')
                stopBar(ctx, 0);
            }, delay * 100);
        }
        //     {
        //     setTimeout(() => document.querySelector(`#my_canvas${getCanvasId(person.id) + 4}`).remove(), delay * 100);
        //     setTimeout(() => document.querySelector(`.error${getCanvasId(person.id) + 4}`).style.display = "block", delay * 100);
        // }
    })
}

function clientInterview(person) {
    // const delay = getRandom(1, 2) * 10;
     const delay = getRandom(5, 10) * 10;
    let englishLevelAccept = ["C1", "C2", "B1", "B2"]
    ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 5}`).getContext('2d')
    startBar(ctx, delay);
    return new Promise((resolve, reject) => {
        if (englishLevelAccept.includes(person.english)) {
            setTimeout(() => resolve(person.name), delay * 100);
        } else
        {
            setTimeout(() => {
                ctx = document.querySelector(`#my_canvas${getCanvasId(person.id) + 5}`).getContext('2d')
                stopBar(ctx, 0);
            }, delay * 100);
        }
        //     {
        //     setTimeout(() => document.querySelector(`#my_canvas${getCanvasId(person.id) + 5}`).remove(), delay * 100);
        //     setTimeout(() => document.querySelector(`.error${getCanvasId(person.id) + 5}`).style.display = "block", delay * 100);
        // }
    })
}






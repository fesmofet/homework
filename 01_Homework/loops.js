module.exports = {
    //Task 1
    sumFrom1To99: function () {
        var i = 1;
        var sum = 0;

        for (; i < 100; i++) {
            sum += i;
        }
        return sum;
    },
    //Task 2
    checkPrimeNumber: function (prime) {
        if (typeof prime !== 'number' || prime < 1) {
            return false;
        }
        var isPrime = 'prime number';

        for (var i = 2; i < prime; i++) {
            if (prime % i === 0) {
                isPrime = 'not prime number';
            }
        }
        return isPrime;
    },
    //Task 3
    findSquareRoot: function(value) {
        if (typeof value !== 'number') {
            return false;
        }
        for (var i = 0; i < value; i++) {
            if (i * i > value) {
                value = i - 1;
            }
        }
        return value;
    },

    findSquareRoot2: function(value) {
        if (typeof value !== 'number') {
            return false;
        }
        var start = 0;
        var end = value - 1;
        var root = 'error';
        var found = false;
        var middle;

        while (found === false && start <= end) {
            middle = Math.floor((start + end) / 2);
            if (middle * middle === value) {
                found = true;
                root = middle;
            } else if (middle * middle > value && middle * middle < value + (2 * middle)) {
                found = true;
                middle -= 1;
                root = middle;
            } else if (middle * middle > value) {
                end = middle - 1;
            } else {
                start = middle + 1;
            }
        }
        return root;
    },
    //Task 4
    findFactorial: function (value) {
        if (typeof value !== 'number') {
            return false;
        }
        var factorial = 1;
        while (value) {
            factorial *= value;
            value --;
        }
        return factorial;
    },
    //Task 5
    calcSumOfDigits: function (value) {
        if (typeof value !== 'number') {
            return false;
        }
        var sum = 0, remainder;
        while (value) {
            remainder = value % 10;
            value = (value - remainder) / 10;
            sum += remainder;
        }
        return sum;
    },
    //Task 6
    getMirrorNumber: function (value) {
        if (typeof value !== 'number') return false;
        var remainder;
        var mirror = 0;

        while(value) {
            remainder = value % 10;
            value = (value - remainder) / 10;
            mirror += remainder;
            value && (mirror *= 10);
        }
        return mirror;
    }
};
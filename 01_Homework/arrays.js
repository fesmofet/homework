module.exports = {
    //Task 1
    findMinElArr: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        for (var i = 0; i < arr.length; i++) {
            if (arr[i] < arr[0]) {
                arr[0] = arr[i];
            }
        }
        return arr[0];
    },
    //Task 2
    findMaxElArr: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        for (var i = 0; i < arr.length; i++) {
            if (arr[i] > arr[0]) {
                arr[0] = arr[i];
            }
        }
        return arr[0];
    },
    //Task 3
    findIndexOfMin: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var minIndex = 0;

        for (var i = 1; i < arr.length; i++) {
            if (arr[i] < arr[minIndex]) {
                minIndex = i;
            }
        }
        return minIndex;
    },
    //Task 4
    findIndexOfMax: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var maxIndex = 0;

        for (var i = 1; i < arr.length; i++) {
            if (arr[i] > arr[maxIndex]) {
                maxIndex = i;
            }
        }
        return maxIndex;
    },
    //Task 5
    calcSumOddIndex: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var sum = 0;

        for (var i = 0; i < arr.length; i++) {
            if (i % 2 !==0) {
                sum += arr[i];
            }
        }
        return sum;
    },
    //Task 6
    reverseArr: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var reverse = new Array;

        for(var i = arr.length-1; i >= 0; i--) {
            reverse.push(arr[i]);
        }

        return reverse;
    },
    //Task 7
    calcSumOddIndexEl: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var sum = 0;
        for (var i = 0; i < arr.length; i++) {
            if (i % 2 !==0)
                sum++;
        }
        return sum;
    },
    //Task 8
    halfReverse: function (arr) {
        if (!Array.isArray(arr)) return false;

        var halfLength = Math.floor(arr.length /2);
        var centerPosition = halfLength + arr.length % 2;

        for (var i = 0; i < halfLength; i++) {
            var temp = arr[i];
            arr[i] = arr[centerPosition + i];
            arr[centerPosition + i] = temp;
        }
        return arr;
    },
    //Task 9
    sortBubble: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }
        var temp;

        for (var i = 1; i < arr.length; i++) {
            for (var j = 1; j < arr.length; j++) {
                if (arr[j] < arr[j-1]) {
                    temp = arr[j];
                    arr[j] = arr[j-1];
                    arr[j-1] = temp;
                }
            }
        }
        return arr;
    },
    sortSelect: function (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }

        var min;
        var temp;

        for (var i = 0; i < arr.length; i++) {
            min = i;
            for (var j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[min]) {
                    min = j;
                }
            }
            if (min != i) {
                temp = arr[i];
                arr[i] = arr[min];
                arr[min] = temp;
            }
        }
        return arr;
    },
    sortInsert: function (arr) {
        if (!Array.isArray(arr)) return false;

        var temp;
        var j;

        for (var i = 1; i < arr.length; i++) {
            temp = arr[i];
            j = i;
            while (j > 0 && arr[j-1] > temp) {
                arr[j] = arr[j-1];
                j = j - 1;
            }
            arr[j] = temp;
        }
        return arr;
    },
    //Task 10
    sortMerge: function mergeSort (arr) {
        if (!Array.isArray(arr)) {
            return false;
        }
        function merge (left, right) {

            var result = [];
            var indexLeft = 0;
            var indexRight = 0;

            while (indexLeft < left.length && indexRight < right.length) {
                if (left[indexLeft] < right[indexRight]) {
                    result.push(left[indexLeft]);
                    indexLeft++;
                } else {
                    result.push(right[indexRight]);
                    indexRight++;
                }
            }

            return result.concat(left.slice(indexLeft)).concat(right.slice(indexRight));
        }
        if (arr.length < 2) {
            return arr;
        }

        var middle = Math.floor(arr.length / 2);
        var left = arr.slice(0, middle);
        var right = arr.slice(middle);

        return merge(mergeSort(left), mergeSort(right));
    },

    sortQuick: function quicksort(array) {
        if (!Array.isArray(array)) {
            return false;
        }
        if (array.length === 0) {
            return [];
        }

        var left = [];
        var right = [];
        var pivot = array[0];

        for (var i = 1; i < array.length; i++) {
            if(array[i] < pivot) {
                left.push(array[i]);
            } else {
                right.push(array[i]);
            }
        }

        return quicksort(left).concat(pivot, quicksort(right));
    }
};
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Yo-yo Borik in da house!';
  todos = [
    {
      label: 'Bring milk',
      priority: 3
    },
    {
      label: 'Bring cofee',
      priority: 10
    },
    {
      label: 'Clean house',
      priority: 9
    },
    {
      label: 'Fix Bulb',
      priority: 3
    }
  ];

  addTodo(newTodoLabel, newPriority) {
    var newTodo = {
      label: newTodoLabel,
      priority: !newPriority ? Math.floor(Math.random() * 10) + 1 : newPriority,
    };
    this.todos.push(newTodo);
  }

  deleteTodo(todo) {
    this.todos = this.todos.filter(t => t.label !== todo.label)
  }
}

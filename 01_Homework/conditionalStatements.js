module.exports = {
    //Task 1
    checkEvenOrOdd:  function (value1, value2) {
        if (typeof value1 !== 'number' || typeof value2 !== 'number') {
            return false;
        }
        return value1 % 2 === 0 ? value1 * value2 : value1 + value2;
    },
    //Task 2
    determineLocOfSpot: function (x, y) {
        if (typeof x !== 'number' || typeof y !== 'number') {
            return false;
        }

        if (x < 0 && y > 0) {
            return '1st quarter';
        } else if (x > 0 && y > 0) {
            return '2nd quarter';
        } else if (x < 0 && y < 0) {
            return '3rd quarter';
        } else if (x > 0 && y < 0) {
            return '4th quarter';
        } else if (x === 0 && y !== 0) {
            return 'y axis';
        } else if (x !== 0 && y === 0) {
            return 'x axis';
        } else if (x === 0 && y === 0) {
            return 'cross point';
        }
    },
    //Task 3
    getSumOfPositive: function (a, b, c) {
        if (typeof a !== 'number' || typeof b !== 'number' || typeof c !== 'number') {
            return false;
        }
        var sum = 0;
        if (a > 0) {
            sum += a;
        }
        if (b > 0) {
            sum += b;
        }
        if (c > 0) {
            sum += c;
        }
        return sum;
    },
    //Task 4
    countExpression: function (a, b, c) {
        if (typeof a !== 'number' || typeof b !== 'number' || typeof c !== 'number') {
            return false;
        }
        var multiply = a * b * c;
        var sum = a + b + c;
        return multiply > sum ? multiply + 3 : sum + 3;
    },
    //Task 5
    getGrade: function (rate) {
        if (typeof rate !== 'number') {
            return false;
        }
        var result = 'Error';
        if (rate < 0 || rate > 100) {
            return result;
        } else if (rate < 20) {
            result = 'F'
        } else if (rate < 40) {
            result = 'E'
        } else if (rate < 60) {
            result = 'D'
        } else if (rate < 75) {
            result = 'C'
        } else if (rate < 90) {
            result = 'B'
        } else if (rate <= 100) {
            result = 'A'
        }
        return result;
    }
};